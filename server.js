var express = require('express');
var sapui5 = require('sapui5-runtime');
var app = express();
app.use("/resources", express.static(sapui5));
app.use(express.static(__dirname + "/webapp"));
app.listen("9999");


var cors_proxy = require('cors-anywhere');
var host = '0.0.0.0';
var port = 9998;
cors_proxy.createServer({
  originWhitelist: [],
  requireHeader: ['origin', 'x-requested-with'],
  removeHeaders: ['cookie', 'cookie2']
}).listen(port, host, function () {
  console.log('Running CORS Anywhere on ' + host + ':' + port);
});